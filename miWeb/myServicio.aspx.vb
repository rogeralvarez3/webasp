﻿Public Class myServicio
    Inherits System.Web.UI.Page
    Dim cn As New OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + HttpRuntime.AppDomainAppPath + "\NEPTUNO.mdb")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim result As String = ""
        'En ves del form normal, aquí se trabaja con el objeto page que está referenciado arriba
        'este es el servicio así que solo contestará peticiones

        Dim petición As String = Request.QueryString("petición") 'aquí extrae la petición cuando le  llegue, en el evento load

        'Response.Write("Recibí tu petición, ... es ésta: " + petición) 'y aquí responde al html
        If petición = "quiero ver los productos" Then
            result = mostrarProductosJSON()
        ElseIf petición = "...cualquier otra cosa" Then
            'aquí la respuesta...
        End If
        Response.Write(result)
    End Sub
    'a este pobre ya no lo llamaré
    Private Function mostrarProductos() As String
        Dim result As String = ""
        Dim rd As OleDb.OleDbDataReader = Nothing, cmd As OleDb.OleDbCommand
        cn.Open()
        cmd = cn.CreateCommand()
        cmd.CommandText = "select id,[Código de producto],[Nombre del producto] from productos"
        Try
            rd = cmd.ExecuteReader
            For i As Integer = 0 To rd.FieldCount - 1
                result += "<th>" + rd.GetName(i) + "</th>"
            Next
            result = "<tr>" + result + "</tr>"
            While rd.Read
                Dim fila As String = ""
                For i2 As Integer = 0 To rd.FieldCount - 1
                    fila += "<td>" + rd.GetValue(i2).ToString + "</td>"
                Next
                result += "<tr>" + fila + "</tr>"
            End While
            result = "<table>" + result + "</table>"
        Catch ex As Exception
            result = ex.Message
        End Try
        rd.Close()
        cn.Close()
        Return result
    End Function
    'a este sí
    Private Function mostrarProductosJSON() As String
        Dim result As String = ""
        Dim rd As OleDb.OleDbDataReader = Nothing, cmd As OleDb.OleDbCommand
        cn.Open()
        cmd = cn.CreateCommand()
        cmd.CommandText = "select id,[Código de producto],[Nombre del producto] from productos"
        Try
            rd = cmd.ExecuteReader
            'En esta parte ya no tengo que mandar el resultado con formato de tabla
            'así me ahorro datos en la transferencia, solo enviaré los datos en formato json
            While rd.Read
                Dim fila As String = ""
                For i2 As Integer = 0 To rd.FieldCount - 1
                    fila += Chr(34) + rd.GetName(i2) + Chr(34) + ":" + Chr(34) + rd.GetValue(i2).ToString + Chr(34) + ","
                Next
                result += "{" + Left(fila, fila.Length - 1) + "},"
            End While
            result = "[" + Left(result, result.Length - 1) + "]" 'la función left solo me quita la coma que sobró en cada bucle
        Catch ex As Exception
            'result = ex.Message
            'hasta el error lo devolveré en formato json, para usar una librería llamada sweetalert2
            result = "[{" + Chr(34) + "type" + Chr(34) + ":" + Chr(34) + "error" + Chr(34) + "," + Chr(34) + "text" + Chr(34) + ":" + Chr(34) + ex.Message.ToString.Replace(Chr(34), "'") + Chr(34) + "}]"
            'te preguntarás por qué uso tantos chr(34), es porque en el json tienen que ir
            'comillas dobles y pues vb interpreta las comillas dobles solo para string
            'y no puedo poner comillas dobles dentro de comillas, así que llamo a su valor ascii

        End Try
        rd.Close()
        cn.Close()
        Return result
    End Function

End Class